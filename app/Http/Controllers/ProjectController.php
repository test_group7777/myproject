<?php

namespace App\Http\Controllers;
use Illuminate\View\View;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
public function oneProj():View
{
    return view('proekt-1.index');
}

public function twoProj():View
{
    return view('proekt-2.index');
}
    public function threeProj():View
    {
        return view('proekt-3.index');
    }

    public function fourProj():View
    {
        return view('proekt-4.index');
    }

    public function fiveProj():View
    {
        return view('proekt-5.index');
    }
}
