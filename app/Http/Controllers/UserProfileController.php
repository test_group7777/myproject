<?php

namespace App\Http\Controllers;

use App\Models\UserProfile;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;


class UserProfileController extends Controller
{

    public function index():View
    {
        $fileProfile = UserProfile::all();
        return view('proekt.UserProfile', ['fileProfile' => $fileProfile]);
    }

    public function create(Request $request):RedirectResponse
    {
        $this->validate($request, [
            'first_name' => 'required|min:2|max:50',
            'last_name' => 'required|min:2|max:50',
        ]);
        $user = $request->user();
        $userProfile = new UserProfile;
        $userProfile->first_name = $request->input('first_name');
        $userProfile->last_name = $request->input('last_name');
        $userProfile->user_id = $user->id;
        $userProfile->save();

        return redirect(route('profile.index'));
    }

    public function delete(int $id):RedirectResponse
    {
        UserProfile::find($id)->delete();
        return redirect(route('profile.index'));
    }

    public function edit(int $id):View
    {
        $user = UserProfile::find($id);
        return view('proekt.edit', ['user' => $user]);
    }

    public function update(Request $request,int $id):RedirectResponse
    {
        $test = UserProfile::find($id);

        $test->first_name = $request->first_name;
        $test->last_name = $request->last_name;
        $test->save();
        return redirect(route('profile.index'));
    }

    public function show(int $id):View
    {
        $show = UserProfile::find($id);
        return view('proekt.show',['show'=>$show]);
    }
}
