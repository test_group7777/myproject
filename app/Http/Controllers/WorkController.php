<?php

namespace App\Http\Controllers;

use App\Forum;
use App\Images;
use App\Models\UserProfile;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use function GuzzleHttp\Promise\all;

class WorkController extends Controller
{
    public function index():View
    {
        return view('proekt.index');
    }

    public function we():View
    {
        return view('proekt.page-2');
    }

    public function service():View
    {
        return view('proekt.page-3');
    }

    public function news():View
    {
        return view('proekt.page-4');
    }

    public function projects():View
    {
        return view('proekt.projects');
    }

    public function contact():View
    {
        return view('proekt.page-5-contacts');
    }

    public function gallery(Request $request):View
    {
        $array = $request->user()->images->pluck('img');
        $test = $request->user()->images()->paginate(10);

        return view('proekt.gallery', ['path' => '', 'array' => $array, 'test' => $test]);
    }

    public function upload(Request $request):RedirectResponse
    {
        $path = $request->file('img')->store('upload', 'public');
        $request->user()->images()->create(['img' => $request->file('img')->hashName()]);

        return redirect('gallery')->with('path', $path);
    }

    public function forum():View
    {
        $test = Forum::paginate(5);
        return view('proekt.forum',['test' => $test]);
    }

    public function comment(Request $request):RedirectResponse
    {
        $test = Forum::all();
      $request->user()->forums()->create($request->input());
        return redirect(route('forum'));
    }


}
