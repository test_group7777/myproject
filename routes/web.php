<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'middleware' => 'auth'
], function(){
    Route::get('/gallery', [\App\Http\Controllers\WorkController::class ,  'gallery'])->name('gallery');

    Route::post('/image/upload', [\App\Http\Controllers\WorkController::class ,  'upload'])->name('upload');

    Route::get('/forum', [\App\Http\Controllers\WorkController::class , 'forum'])->name('forum');

    Route::post('/save/comment', [\App\Http\Controllers\WorkController::class ,  'comment'])->name('comment');
});

Route::get('/',[\App\Http\Controllers\WorkController::class , 'index'])->name('index');

Route::get('/we',[\App\Http\Controllers\WorkController::class ,  'we'])->name('we');

Route::get('/services', [\App\Http\Controllers\WorkController::class ,  'service'])->name('service');

Route::get('/news', [\App\Http\Controllers\WorkController::class ,  'news'])->name('news');

Route::get('/projects', [\App\Http\Controllers\WorkController::class ,  'projects'])->name('projects');

Route::get('/contact', [\App\Http\Controllers\WorkController::class ,  'contact'])->name('contact');

Route::get('/user/profile', [\App\Http\Controllers\UserProfileController::class ,'index'])->name('profile.index');

Route::post('/user/create', [\App\Http\Controllers\UserProfileController::class ,'create'])->name('profile.create');

Route::get('/user/{id}/delete', [\App\Http\Controllers\UserProfileController::class,'delete'])->name('profile.delete');

Route::get('user/{id}/edit', [\App\Http\Controllers\UserProfileController::class,'edit'])->name('profile.edit');

Route::post('user/{id}/update', [\App\Http\Controllers\UserProfileController::class ,'update'])->name('profile.update');

Route::get('user/{id}/show', [\App\Http\Controllers\UserProfileController::class ,'show'])->name('profile.show');

Route::get('/home', [\App\Http\Controllers\HomeController::class  , 'index'])->name('home');


\Illuminate\Support\Facades\Auth::routes();



Route::get('/project-1',[\App\Http\Controllers\ProjectController::class,'oneProj'])->name('project-1');
Route::get('/project-2',[\App\Http\Controllers\ProjectController::class,'twoProj'])->name('project-2');
Route::get('/project-3',[\App\Http\Controllers\ProjectController::class,'threeProj'])->name('project-3');
Route::get('/project-4',[\App\Http\Controllers\ProjectController::class,'fourProj'])->name('project-4');
Route::get('/project-5',[\App\Http\Controllers\ProjectController::class,'fiveProj'])->name('project-5');


