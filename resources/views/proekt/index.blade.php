@extends('layout')
@section('content')

    <div class="page">

    <div id="strip"></div>
    <div class="page-text">
        <p>Передовая IT студия</p>
        <h2 style="text-align: center;font-family: sans-serif;font-size: 48px;color: white;">Мы создаем легкие решения
            сложных задач и проблем</h2>
        <div class="page-a">
            <a href="{{route('projects')}}">Наши проекты</a>
        </div>
    </div>
    </div>
@endsection
