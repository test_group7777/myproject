@extends('layout')
@section('content')
    <div class="project">
        <div class="project-content">
            <h1>Наши проекты</h1>
            <p style="margin-left: 350px;font-size: 16px;line-height: 25px;width: 730px;height: 41px;">Lorem Ipsum - это
                текст-"рыба", часто используемый в печати и вэб-дизайне.
                Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. </p>
            <div style="margin-top: 50px;" class="projects-photos">
                <a href="{{route('project-1')}}"><img src="../../image/11.png" width="300" height="300"></a>
                <a href="{{route('project-2')}}"> <img src="../../image/15.png" width="300" height="300"></a>
                <a href="{{route('project-3')}}"> <img src="../../image/12.png" width="300" height="300"></a>
                <a href="{{route('project-4')}}"> <img src="../../image/13.png" width="300" height="300"></a>
                <a href="{{route('project-5')}}"> <img src="../../image/14.png" width="300" height="300"></a>
            </div>
        </div>
    </div>
@endsection
