@extends('layout')
@section('content')
    <div class="page-4">
        <h3 style="margin-left: 70px;padding-top: 150px;width: 500px ;font-size: 36px;font-family: sans-serif;color: white; line-height: 60px;">
            Наши работы, на которых
            мы специализируемся</h3>
        <a id="page-4-a" href="{{route('service')}}">Все услуги</a>
            <div class="page-4-content">
            <?php for ($i = 0; $i < 3; $i++): ?>
            <div style="margin:40px 0 0 70px;" class="page-4-box">
                <img style="border-radius: 50px;" src="../../image/5.jpeg" width="350px" height="300" >
                <h5 style="line-height: 25px;">01. Исследование и упаковка</h5>
                <p style="font-size: 16px;line-height: 25px;color: #999999;">Lorem Ipsum - это текст-"рыба",
                    часто используемый в печати и вэб-дизайне.
                    Lorem Ipsum является стандартной "рыбой" для
                    текстов на латинице с начала XVI века.</p>
              <div style="margin-top: 50px;width: 97px;height: 20px;" id="page-4-info">
                <a style="text-decoration: none;color: blue;" href="#"><b>Подробнее</b></a>
              </div>
            </div>

            <?php endfor; ?>
        </div>
    </div>
@endsection
