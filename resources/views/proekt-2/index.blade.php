@extends('layout')
@section('content')
<style>
    .topBar {
    display: flex;
    width: 100%;
    height: 61px;
    background: #09364B;
    padding: 10px 0 0;
    font-size: 14px;
    }

    .topBackAndDownload {
    display: flex;
    width: 100%;
    height: 47px;
    background: #0D1326;
    color: white;
    font-family: test-font;
    }

    .Back, .download, .Buy {
    padding-top: 15px;
    }

    .download {
    margin-left: auto;
    padding-right: 50px;
    }

    .topBackAndDownload a {
    text-decoration: none;
    color: #94959e;
    transition: 1s;

    }

    .topBackAndDownload a:hover {
    color: white;
    }

    .img-phone {
    margin-left: auto;
    padding-right: 15px;
    }

    .LogoMainAndSearchBasket {
    display: flex;
    padding: 25px;
    justify-content: space-between;
    }

    .menu {
    display: flex;
    width: 100%;
    height: 42px;
    background: #4CCFC1;
    border: #30938a 1px solid;
    justify-content: space-evenly;
    color: white;
    font-size: 23px;
    flex-wrap: wrap;
    }

    .menu a {
    color: white;
    text-decoration: white;
    font-family: test2-font;
    margin: 10px 12px 10px 0;
    font-size: 23px;
    transition: 1s;
    }

    .menu a:hover {
    color: #94959e;
    }

    .SellShop {
        display: flex;
        width: 100%;
        height: 337px;
        background: #4CCFC1;
    }

    .boxing {
        margin: 64px 0;
        display: flex;
        width: 1215px;
        height: 337px;
        justify-content: space-around;
    }

    .T-Shirt1-Destruction {
        margin: 48px 0 0;
        width: 605px;
        text-align: center;
        font-family: test-font;
        color: white;
    }

    .Name {
        font-size: 66px;
        margin: 0 0 30px;
        font-family: test2-font;
    }

    .sell-products {
        /*width: 100%;*/
        height: 848px;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-evenly;
        padding: 24px;
    }

    .box-product {
        display: flex;
        width: 342px;
        height: 341px;
        border: 1px #958e8e solid;
        justify-content: center;
        border-bottom: 7px solid #4CCFC1;
        flex-wrap: wrap;
    }

    .box-product img {
        margin: 26px 0 0;
    }

    .box-product h4 {
        width: 328px;
        height: 32px;
        color: #94959e;
        font-family: test2-font;
        font-size: 29px;
        text-align: center;
        margin: 20px 0 26px;
    }
    .box-product h4 {
        color:#94959e ;
        font-size: 29px;
    }

    #INDULGE{
        font-family: test2-font;
        color: white;
        margin: 0 0 0 30px;
        padding: 8px 20px;
        font-size: 26px;
        width: 50px;
        height: 45px;
        text-align: center;
        background: #48c485;
    }
    a {
        text-decoration: none;
    }
    .footer {
        height: 361px;
        display: flex;
        background: #4CCFC1;
    }

    .footerMenu {

        flex-direction: row;
        display: flex;
        justify-content: space-between;
        width: 1215px;
        height: 361px;

    }

    .footerBox {
        padding: 24px;
        flex-direction: column;
        display: flex;
        width: 248px;
        height: 265px;
        /*margin: 0 0 0 40px;*/
    }

    .footerMenu a {
        text-align: center;
        text-decoration: none;
        color: white;
        font-size: 16px;
        font-family: test2-font;
        padding: 4px;
    }

    .footerMenu h4 {
        font-family: test2-font;
        font-size: 28px;
    }

    .endFooter {
        display: flex;
        width: 100%;
        justify-content: center;
        color: white;
        height: 77px;
        background: #00a99a;
    }
    .endFooter p {
        margin-top: auto;
        padding-bottom: 25px;
    }
    #slides {
        display: flex;
        position: relative;
        width: 100%;
        height: 480px;
        padding: 0px;
        margin-top: 0;
        list-style-type: none;

    }
    .slide {
        display: flex;
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 500px;
        opacity: 0;
        z-index: 1;
        -webkit-transition: opacity 1s;
        -moz-transition: opacity 1s;
        -o-transition: opacity 1s;
        transition: opacity 1s;
    }

    .featured {

        display: flex;
        border: 1px black solid;
        background: #4CCFC1;
        color: white;
    }
    .showing {
        opacity: 1;
        z-index: 2;
    }

    .slide:nth-of-type(1) {
        background: #4CCFC1;
    }
    .slide:nth-of-type(2) {
        background: #4CCFC1;
    }
    .slide:nth-of-type(3) {
        background: #4CCFC1;
    }
    .slide:nth-of-type(4) {
        background: #4CCFC1;
    }
    </style>

<div class="topBar">
    <div class="logo">
        <img src="../../../proekt-2/img/logo.png">
    </div>
    <div class="img-phone">
        <img src="../../../proekt-2/img/monitor%20(1).png">
        <img src="../../../proekt-2/img/ipad-icon.png">
        <img src="../../../proekt-2/img/ipad-landscape.png">
        <img src="../../../proekt-2/img/iphone-icon.png">
        <img src="../../../proekt-2/img/iphone-landscape.png">
        <img src="../../../proekt-2/img/nokia-icon.png">
    </div>
    <button type="submit" style="height: 32px; width: 182px; color: white;background: #0378a3;margin-right: 30px">
        Download Template
    </button>
</div>
<div class="topBackAndDownload">
    <div class="Back">
        <a href="#" style="padding: 15px 35px;"><b>Back</b></a>
    </div>
    <div class="download">
        <a href="#" style="margin-right: 50px"><b>Download</b></a>
        <a href="#"><b>Buy Now</b></a>
    </div>
</div>

<div class="LogoMainAndSearchBasket">
    <div class="LogoMain">
        <img src="../../../proekt-2/img/logo%20(1).png">
    </div>
    <div class="Search">
        <input type="text" style="height: 32px; width: 428px">
    </div>
    <div class="Basket">
        <img src="../../../proekt-2/img/photo_2020-08-01_23-31-05.jpg">
    </div>
</div>

<div class="menu">
    <a href="#">HOME |</a>
    <a href="#">SALE |</a>
    <a href="#">HANDBAGS |</a>
    <a href="#">ACCESSORIES |</a>
    <a href="#">WALLETS |</a>
    <a href="#">SALE |</a>
    <a href="#">MENS STORE |</a>
    <a href="#">SHOES |</a>
    <a href="#">VINTAGE |</a>
    <a href="#">SERVICES |</a>
    <a href="#">CONTACT US |</a>
</div>
<ul id="slides">
    <li class="slide showing">
        <div class="SellShop">
            <div class="boxing">
                <div class="T-Shirt">
                    <img src="../../../proekt-2/img/slider1.png">
                </div>
                <div class="T-Shirt1-Destruction">
                    <p class="Name">WELCOME TO ADITII</p>
                    <p style="font-size: 16px; height: 78px;">When she reached the first hills of the Italic Mountains,
                        she had a last view back on the skyline of her hometown Bookmarksgrove,
                        the headline of Alphabet Village and the subline of her own road.</p>
                </div>
            </div>
        </div>
    </li>
    <li class="slide">
        <div class="SellShop">
            <div class="boxing">
                <div class="T-Shirt">
                    <img src="../../../proekt-2/img/slider2.png">
                </div>
                <div class="T-Shirt1-Destruction">
                    <p class="Name">Easy management</p>
                    <p style="font-size: 16px; height: 78px;">Far far away,
                        behind the word mountains, far from the countries Vokalia and Consonantia,
                        there live the blind texts.
                        Separated they live in Bookmarksgrove right at the coast.</p>
                </div>
            </div>
        </div>
    </li>
    /li>
    <li class="slide">
        <div class="SellShop">
            <div class="boxing">
                <div class="T-Shirt">
                    <img src="../../../proekt-2/img/slider3.png">
                </div>
                <div class="T-Shirt1-Destruction">
                    <p class="Name">Revolution</p>
                    <p style="font-size: 16px; height: 78px;">A small river named Duden flows by their place
                        and supplies it with the necessary regelialia.
                        It is a paradisematic country,
                        in which roasted parts of sentences fly.</p>
                </div>
            </div>
        </div>
    </li>
    </li>
    <li class="slide">
        <div class="SellShop">
            <div class="boxing">
                <div class="T-Shirt">
                    <img src="../../../proekt-2/img/slider4.png">
                </div>
                <div class="T-Shirt1-Destruction">
                    <p class="Name">Quality Control</p>
                    <p style="font-size: 16px; height: 78px;">Even the all-powerful Pointing has no control
                        about the blind texts it is an almost unorthographic
                        life One day however a small line of blind text by the name.</p>
                </div>
            </div>
        </div>
    </li>

</ul>

<div class="featured" style="margin-top: 20px; font-family: test2-font;font-size: 32px;padding: 24px;">
    <h2> FEATURED PRODUCTS </h2>
</div>
<div class="sell-products">
    <a href="#" class="a">
        <div class="box-product">
            <img src="../../../proekt-2/img/img-sell/1.jpg" width="174" height="116">
            <h4>BRANDED BAGS</h4><h4>$300
                <span id="INDULGE"> INDULGE</span></h4>
        </div>
    </a>
    <a href="#"><div class="box-product">
            <img src="../../../proekt-2/img/img-sell/2.jpg" width="116" height="116">
            <h4>BRANDED BAGS</h4><h4>$300
                <span id="INDULGE"> INDULGE</span></h4>
        </div>
    </a>
    <a href="#"><div class="box-product">
            <img src="../../../proekt-2/img/img-sell/3.jpg" width="116" height="116">
            <h4>BRANDED BAGS</h4><h4>$300
                <span id="INDULGE"> INDULGE</span></h4>
        </div>
    </a>
    <a href="#"><div class="box-product">
            <img src="../../../proekt-2/img/img-sell/4.jpg" width="168" height="126">
            <h4>BRANDED BAGS</h4><h4>$300
                <span id="INDULGE"> INDULGE</span></h4>
        </div>
    </a>
    <a href="#"><div class="box-product">
            <img src="../../../proekt-2/img/img-sell/5.jpg" height="126" width="140">
            <h4>BRANDED BAGS</h4><h4>$300
                <span id="INDULGE"> INDULGE</span></h4>
        </div>
    </a>
    <a href="#"><div class="box-product">
            <img src="../../../proekt-2/img/img-sell/6.jpg" width="120" height="128">
            <h4>BRANDED BAGS</h4><h4>$300
                <span id="INDULGE"> INDULGE</span></h4>
        </div>
    </a>
</div>
<div class="footer">
    <div class="footerMenu">
        <div class="footerBox">
            <a href="#"><h4>FEATURED SALE</h4></a>
            <a href="#">Alexis Hudson</a>
            <a href="#"> American Apparel</a>
            <a href="#"> Ben Sherman</a>
            <a href="#"> Big Buddha</a>
            <a href="#"> Channel</a>
            <a href="#"> Christian Audigier</a>
            <a href="#"> Coach</a>
            <a href="#"> Cole Haan</a>
        </div>
        <div class="footerBox">
            <a href="#"><h4>MENS STORE</h4></a>
            <a href="#">Alexis Hudson</a>
            <a href="#">American Apparel</a>
            <a href="#"> Ben Sherman</a>
            <a href="#"> Big Buddha</a>
            <a href="#"> Channel</a>
            <a href="#">Christian Audigier</a>
            <a href="#"> Coach</a>
            <a href="#"> Cole Haan</a>
        </div>
        <div class="footerBox">
            <a href="#"><h4>WOMEN STORE</h4></a>
            <a href="#">Alexis Hudson</a>
            <a href="#"> American Apparel</a>
            <a href="#"> Ben Sherman</a>
            <a href="#"> Big Buddha</a>
            <a href="#"> Channel</a>
            <a href="#"> Christian Audigier</a>
            <a href="#"> Coach</a>
            <a href="#"> Cole Haan</a>
        </div>
        <div class="footerBox">
            <a href="#"><h4> QUICK LINKS</h4></a>
            <a href="#"> Alexis Hudson</a>
            <a href="#"> American Apparel</a>
            <a href="#"> Ben Sherman</a>
            <a href="#"> Big Buddha</a>
            <a href="#"> Channel</a>
            <a href="#"> Christian Audigier</a>
            <a href="#"> Coach</a>
            <a href="#"> Cole Haan</a>
        </div>
    </div>
</div>
<div class="endFooter">
    <p style="margin-top: auto;">© 2014 Aditii. All rights reserved | Template by W3Layouts</p>
</div>
<script src="{{ \Illuminate\Support\Facades\URL::asset('/js/app.js')}}"></script>
@endsection
